/********************************************************************
    created:    2013/04/02
    created:    2:4:2013    14:01
    filename:   freetype\src\fet\fet.c
    file path:  freetype\src\fet
    file base:  fet
    file ext:   c
    author:   DMC R&D Graphics Lab
*********************************************************************/

#include <ft2build.h>

#include "sfenhancer.c"
#include "sfreshaper.c"

/* END */
