package com.sec.chromium.content.browser.pipette.suctionanimator;

public class SimulationConstants {

    /* Some physics constants */
    public static final float DAMPING = 0.1f; // 0.01f; //0.3f
    public static final float TIME_STEPSIZE2 = 0.5f * 0.5f;
    public static final int CONSTRAINT_ITERATIONS = 1;

}
