package com.sec.android.app.sbrowser.firefox;

public interface ProgressDisplay {
    public void showProgress();
    public void dismissProgress();
  }
